$(document).ready(function () {
    let images = $(".ar-head__body-img");
    for (let img of images) {
        let captionText = $(img).attr('data-caption');
        /*
                упомянуть про то, что если не обернуть объект в $() - то он не будет отвечать на методы jQuery,
                например на метод attr, так как без обёртки он ялвяется обычным элементом DOM
                */
        $(`<span class='ar-head__body-img-capture'>${captionText}</span>`).insertAfter($(img));
    }

    /**
     * slick
     */
    $('.following-article-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });
});
