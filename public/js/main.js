$(document).ready(function () {

    let quill = new Quill('#editor', {
        theme: 'snow'
    });

    $(".save_article").click(function () {
        let data = {
            'title': $(".article__title-text").val(),
            'article': $(".ql-editor").html(),
            'preview_text': $(".preview-text-aria").val(),
        };

        $.ajax({
            type: "POST",
            data: data,
            url: '/admin_/processing_new_article/',
            success: function (data) {
                console.log(data);
            },
            error: function () {
                console.log("noup");
            }
        });
    });

});