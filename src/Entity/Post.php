<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publishTime;

    /**
     * @ORM\Column(type="text")
     */
    private $previewText;

    /**
     * @ORM\Column(type="text")
     */
    private $fullText;

    /**
     * @ORM\Column(type="string")
     */
    private $previewImage;

    /**
     * @ORM\Column(type="integer")
     */
    private $viewsCount;

    /**
     * @ORM\Column(type="integer")
     */
    private $commentsCount;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getPublishTime()
    {
        return $this->publishTime;
    }

    /**
     * @param mixed $publishTime
     */
    public function setPublishTime($publishTime): void
    {
        $this->publishTime = $publishTime;
    }

    /**
     * @return mixed
     */
    public function getPreviewText()
    {
        return $this->previewText;
    }

    /**
     * @param mixed $previewText
     */
    public function setPreviewText($previewText): void
    {
        $this->previewText = $previewText;
    }

    /**
     * @return mixed
     */
    public function getFullText()
    {
        return $this->fullText;
    }

    /**
     * @param mixed $fullText
     */
    public function setFullText($fullText): void
    {
        $this->fullText = $fullText;
    }

    /**
     * @return mixed
     */
    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    /**
     * @param mixed $previewImage
     */
    public function setPreviewImage($previewImage): void
    {
        $this->previewImage = $previewImage;
    }

    /**
     * @return mixed
     */
    public function getViewsCount()
    {
        return $this->viewsCount;
    }

    /**
     * @param mixed $viewsCount
     */
    public function setViewsCount($viewsCount): void
    {
        $this->viewsCount = $viewsCount;
    }

    /**
     * @return mixed
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * @param mixed $commentsCount
     */
    public function setCommentsCount($commentsCount): void
    {
        $this->commentsCount = $commentsCount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

}
