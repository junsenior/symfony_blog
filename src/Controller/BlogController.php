<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\UserFeedBack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function home()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository(Post::class)->findBy(['status' => 'saved']);

        $articles = [];
        $nowDateTime = new \DateTime();
        foreach ($posts as $index => $post) {
            if ($nowDateTime->format("Y") === $post->getPublishTime()->format("Y")
                and $nowDateTime->format("m") === $post->getPublishTime()->format("m")
                and $nowDateTime->format("d") === $post->getPublishTime()->format("d"))
                $currentDateTimeForPost = "сегодня " . $post->getPublishTime()->format("H:i");
            else
                $currentDateTimeForPost = $post->getPublishTime()->format("H:i d-m-Y");
            $buffer = [
                "article_id" => $post->getId(),
                "article_title" => $post->getTitle(),
                "article_preview_img" => $post->getPreviewImage(),
                "article_preview_text" => $post->getPreviewText(),
                "article_author" => $post->getAuthor(),
                "article_date_time" => $currentDateTimeForPost,
                "article_views_count" => $post->getViewsCount(),
                "article_comments_count" => $post->getCommentsCount(),
            ];

            $articles[] = $buffer;
        }

        return $this->render("article/list.html.twig", [
            'title' => 'Главная',
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/p={article_id}", name="show_detail_article")
     */
    public function showDetailArticle($article_id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Post::class)->find($article_id);
        if (!$article) throw $this->createNotFoundException('The article does not exist');

        return $this->render("article/detail_article/detail_article.html.twig", [
            'title' => $article->getTitle(),
            'publish_time' => $article->getPublishTime(),
            'author' => $article->getAuthor(),
            'content' => $article->getFullText(),
            'viewsCount' => $article->getViewsCount()
        ]);
    }

    /**
     * @Route("/admin_/add_news", name="add_news")
     */
    public function addNews()
    {
        return $this->render("article/addnews.html.twig", [
            'title' => 'Добавление поста'
        ]);
    }

    /**
     * @Route("/admin_/processing_new_article/", name="processing_new_article")
     */
    public function processingNewArticle(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $title = $request->request->get("title");
            $article = $request->request->get("article");
            $previewText = $request->request->get("preview_text");
            $username = $this->getUser()->getUsername();
            $dateTime = new \DateTime();

            $post = new Post();
            $post->setAuthor($username);
            $post->setPublishTime($dateTime);
            $post->setCommentsCount(10);
            $post->setViewsCount(10);
            $post->setFullText($article);
            $post->setTitle($title);
            $post->setStatus('saved');
            $post->setPreviewImage("https://trinixy.ru/pics3/20080124/podb/6/krasota_01.jpg");
            $post->setPreviewText($previewText);
            $em->persist($post);
            $em->flush();
            return new JsonResponse([
                'success' => 'yep'
            ]);
        } else
            return new Response("<html><body><img " .
                "src='http://i.viewy.ru/data/photo/9c/53/9c531d669bNKYWMUD_187981_e4d1ae8630.jpg'" .
                "alt='' style='margin: 0 auto; display: block;'></body></html>");
    }

    /**
     * @Route("/success", name="success")
     */
    public function successForm()
    {
        return $this->render("success_form.html.twig", ['title' => 'Успех!']);
    }

    /**
     * @Route("/feedback", name="feedback")
     */
    public function feedback(Request $request)
    {

        $feedback = new UserFeedBack();
        $form = $this->createFormBuilder($feedback)
            ->add('email', EmailType::class)
            ->add('username', TextType::class)
            ->add('text', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Отправить'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $feedback = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($feedback);
            $entityManager->flush();

            return $this->redirectToRoute('success');
        }

        return $this->render('feedback.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Отображение формы'
        ));
    }
}